package com.ffsky.template.demo.data;

import com.ffsky.template.demo.app.ClipActivity;
import com.gitee.hljdrl.ffkit.bean.FFKitLink;
import com.gitee.hljdrl.ffkit.builder.FFKitLinkSource;

import java.util.ArrayList;
import java.util.List;

public class MyDataSource extends FFKitLinkSource {


    @Override
    public List<FFKitLink> getList() {
        List<FFKitLink> list = new ArrayList<>();
        //----------------------------------------------------------------
        list.add(new FFKitLink.Builder().setName("圆角布局").setActivityClass(ClipActivity.class).build());
        return list;
    }


    @Override
    public String getAppName() {
        return "Clip";
    }
}
