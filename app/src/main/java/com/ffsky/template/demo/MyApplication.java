package com.ffsky.template.demo;


import com.ffsky.template.demo.data.MyDataSource;
import com.gitee.hljdrl.ffkit.FFKitApplication;
import com.gitee.hljdrl.ffkit.builder.FFKitLinkSource;

public class MyApplication extends FFKitApplication {


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public Class<? extends FFKitLinkSource> getLinkSource() {
        return MyDataSource.class;
    }
}
