package com.ffsky.template.demo.app;


import android.graphics.Color;
import android.view.View;

import androidx.appcompat.widget.Toolbar;
import com.ffsky.template.demo.R;
import com.ffsky.template.demo.databinding.ActivityClipBinding;
import com.gitee.hljdrl.ffkit.app.FFKitActivity;

public class ClipActivity extends FFKitActivity {

    private ActivityClipBinding binding;

    @Override
    public View getBindingView() {
        binding = ActivityClipBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void initView() {
        Toolbar toolbar = findViewById(R.id.tool_bar);
        toolbar.setTitle("clip");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);//这2行是设置返回按钮的
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.getNavigationIcon().setTint(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void initData() {

    }

    @Override
    public void initListener() {

    }
}
