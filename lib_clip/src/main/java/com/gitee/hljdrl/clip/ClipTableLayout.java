package com.gitee.hljdrl.clip;


import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.gitee.hljdrl.clip.basis.CornersEvent;
import com.gitee.hljdrl.clip.basis.ShapeCorners;


public class ClipTableLayout extends android.widget.TableLayout {

    private ShapeCorners mShapeCorners;

    public ClipTableLayout(Context context) {
        this(context, null);
    }

    public ClipTableLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }


    private void init(Context context, AttributeSet attrs) {
        mShapeCorners = new ShapeCorners();
        mShapeCorners.init(context, attrs, this);
    }

    public CornersEvent getCornersEvent() {
        return mShapeCorners;
    }

    @Override
    public void draw(Canvas canvas) {
        if (getBackground() != null) {
            if (mShapeCorners != null) {
                mShapeCorners.onClipDraw(canvas);
            }
        }
        super.draw(canvas);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        if (mShapeCorners != null) {
            mShapeCorners.onClipDraw(canvas);
        }
        //要在调用super.dispatchDraw之前设置裁切范围噢
        super.dispatchDraw(canvas);
    }
}
