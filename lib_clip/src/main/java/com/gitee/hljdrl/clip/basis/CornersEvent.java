package com.gitee.hljdrl.clip.basis;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

public interface CornersEvent {

    void init(Context context, AttributeSet attrs, View view);

    void onClipDraw(Canvas canvas);

    void setRadius(float radius);

    void setRadius(float leftTopRadius, float rightTopRadius, float leftBottomRadius, float rightBottomRadius);

}
