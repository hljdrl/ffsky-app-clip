package com.gitee.hljdrl.clip;


import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.gitee.hljdrl.clip.basis.CornersEvent;
import com.gitee.hljdrl.clip.basis.ShapeCorners;


public class ClipRelativeLayout extends android.widget.RelativeLayout {

    private ShapeCorners mShapeCorners;

    public ClipRelativeLayout(Context context) {
        this(context, null);
    }

    public ClipRelativeLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ClipRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        mShapeCorners = new ShapeCorners();
        mShapeCorners.init(context, attrs, this);
    }

    public CornersEvent getCornersEvent() {
        return mShapeCorners;
    }

    @Override
    public void draw(Canvas canvas) {
        if(getBackground()!=null){
            if(mShapeCorners!=null){
                mShapeCorners.onClipDraw(canvas);
            }
        }
        super.draw(canvas);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        if(mShapeCorners!=null){
            mShapeCorners.onClipDraw(canvas);
        }
        //要在调用super.dispatchDraw之前设置裁切范围噢
        super.dispatchDraw(canvas);
    }
}
