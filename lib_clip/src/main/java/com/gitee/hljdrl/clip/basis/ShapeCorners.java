package com.gitee.hljdrl.clip.basis;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.gitee.hljdrl.clip.R;

public class ShapeCorners implements CornersEvent {

    private View mView;
    boolean mAntiAlias = true;
    final Path mClipPath = new Path();
    final float[] mRadius = new float[8];

    @Override
    public void init(Context context, AttributeSet attrs, View view) {
        mView = view;
        //------------------------------------------------------------------------------
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ShapeCorners);
        int topLeftRadius = typedArray.getDimensionPixelOffset(R.styleable.ShapeCorners_topLeftRadius, 0);
        int topRightRadius = typedArray.getDimensionPixelOffset(R.styleable.ShapeCorners_topRightRadius, 0);
        int bottomLeftRadius = typedArray.getDimensionPixelOffset(R.styleable.ShapeCorners_bottomLeftRadius, 0);
        int bottomRightRadius = typedArray.getDimensionPixelOffset(R.styleable.ShapeCorners_bottomRightRadius, 0);
        int radius = typedArray.getDimensionPixelOffset(R.styleable.ShapeCorners_clipRadius, 0);
        mAntiAlias = typedArray.getBoolean(R.styleable.ShapeCorners_isAntiAlias, true);
        typedArray.recycle();
        //------------------------------------------------------------------------------
        if (radius > 0) {
            if (topLeftRadius == 0) {
                topLeftRadius = radius;
            }
            if (topRightRadius == 0) {
                topRightRadius = radius;
            }
            if (bottomLeftRadius == 0) {
                bottomLeftRadius = radius;
            }
            if (bottomRightRadius == 0) {
                bottomRightRadius = radius;
            }
        }
        //-------------------------------------------------------
        mRadius[0] = topLeftRadius;
        mRadius[1] = topLeftRadius;
        //
        mRadius[2] = topRightRadius;
        mRadius[3] = topRightRadius;
        //
        mRadius[4] = bottomRightRadius;
        mRadius[5] = bottomRightRadius;
        //
        mRadius[6] = bottomLeftRadius;
        mRadius[7] = bottomLeftRadius;
        //-------------------------------------------------------
    }

    public void setRadius(float radius) {
        mRadius[0] = radius;
        mRadius[1] = radius;
        //
        mRadius[2] = radius;
        mRadius[3] = radius;
        //
        mRadius[4] = radius;
        mRadius[5] = radius;
        //
        mRadius[6] = radius;
        mRadius[7] = radius;
        if(mView!=null){
            mView.invalidate();
        }
    }

    public void setRadius(float leftTopRadius, float rightTopRadius, float leftBottomRadius, float rightBottomRadius) {
        mRadius[0] = leftTopRadius;
        mRadius[1] = leftTopRadius;
        //
        mRadius[2] = rightTopRadius;
        mRadius[3] = rightTopRadius;
        //
        mRadius[4] = leftBottomRadius;
        mRadius[5] = leftBottomRadius;
        //
        mRadius[6] = rightBottomRadius;
        mRadius[7] = rightBottomRadius;
        if(mView!=null){
            mView.invalidate();
        }
    }

    @Override
    public void onClipDraw(Canvas canvas) {
        if (mView == null) {
            return;
        }
        if (mView.getWidth() > 0 && mView.getHeight() > 0) {
            if (mAntiAlias) {
                canvas.setDrawFilter(new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG));
            }
            RectF mRectF = new RectF();
            mRectF.set(0, 0, mView.getWidth(), mView.getHeight());
            //------------------------------------------------------------------------------------
            mClipPath.reset();
            mClipPath.addRoundRect(mRectF, mRadius, Path.Direction.CW);
            canvas.clipPath(mClipPath);
        }
    }
}
