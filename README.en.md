# ffsky-app-clip

#### 介绍
android Layout布局自定义圆角角度



#### 使用说明
<img src="art/art_01.png" width="200" />
<img src="art/art_02.png" width="200" />

1.  引入clip

```gradle
//布局：FrameLayout、LinearLayout、RelativeLayout
api "com.gitee.hljdrl:clip:1.0.1" 

//布局：命名更改，相同类名在android sudio、gradle cache缓存容易造成布局中使用类库的类名红叉
//更改后命名：ClipFrameLayout、ClipLinearLayout、ClipRelativeLayout
//新增：ClipGridLayout、ClipTableLayout
api "com.gitee.hljdrl:clip:1.0.2"
```

2.  布局编写

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent">


    <androidx.appcompat.widget.Toolbar
        android:id="@+id/tool_bar"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="?attr/colorPrimary"
        app:titleTextColor="@android:color/white" />


    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:layout_below="@id/tool_bar">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:orientation="vertical">

            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginBottom="10dp"
                android:background="@android:color/darker_gray"
                android:padding="4dp"
                android:text="clipRadius=20dp" />

            <com.gitee.hljdrl.clip.ClipRelativeLayout
                android:layout_width="100dp"
                android:layout_height="100dp"
                android:layout_gravity="center_horizontal"
                app:clipRadius="20dp">


                <View
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:background="#03A9F4" />

                <View
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:layout_alignParentRight="true"
                    android:background="#673AB7" />

                <View
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:layout_alignParentBottom="true"
                    android:background="#FF5722" />

                <View
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:layout_alignParentRight="true"
                    android:layout_alignParentBottom="true"
                    android:background="#FFEB3B" />

            </com.gitee.hljdrl.clip.ClipRelativeLayout>

            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="10dp"
                android:layout_marginBottom="10dp"
                android:background="@android:color/darker_gray"
                android:padding="4dp"
                android:text="topLeftRadius" />

            <com.gitee.hljdrl.clip.ClipRelativeLayout
                android:layout_width="100dp"
                android:layout_height="100dp"
                android:layout_gravity="center_horizontal"
                app:topLeftRadius="50dp">

                <View
                    android:layout_width="100dp"
                    android:layout_height="100dp"
                    android:background="#00ff00" />
            </com.gitee.hljdrl.clip.ClipRelativeLayout>

            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="10dp"
                android:layout_marginBottom="10dp"
                android:background="@android:color/darker_gray"
                android:padding="4dp"
                android:text="topRightRadius" />

            <com.gitee.hljdrl.clip.ClipRelativeLayout
                android:layout_width="100dp"
                android:layout_height="100dp"
                android:layout_gravity="center_horizontal"
                app:topRightRadius="50dp">

                <View
                    android:layout_width="100dp"
                    android:layout_height="100dp"
                    android:background="#00ff00" />
            </com.gitee.hljdrl.clip.ClipRelativeLayout>

            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="10dp"
                android:layout_marginBottom="10dp"
                android:background="@android:color/darker_gray"
                android:padding="4dp"
                android:text="bottomLeftRadius" />

            <com.gitee.hljdrl.clip.ClipRelativeLayout
                android:layout_width="100dp"
                android:layout_height="100dp"
                android:layout_gravity="center_horizontal"
                app:bottomLeftRadius="50dp">

                <View
                    android:layout_width="100dp"
                    android:layout_height="100dp"
                    android:background="#00ff00" />
            </com.gitee.hljdrl.clip.ClipRelativeLayout>


            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="10dp"
                android:layout_marginBottom="10dp"
                android:background="@android:color/darker_gray"
                android:padding="4dp"
                android:text="bottomRightRadius" />

            <com.gitee.hljdrl.clip.ClipRelativeLayout
                android:layout_width="100dp"
                android:layout_height="100dp"
                android:layout_gravity="center_horizontal"
                app:bottomRightRadius="50dp">

                <View
                    android:layout_width="100dp"
                    android:layout_height="100dp"
                    android:background="#00ff00" />
            </com.gitee.hljdrl.clip.ClipRelativeLayout>


            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="10dp"
                android:layout_marginBottom="10dp"
                android:background="@android:color/darker_gray"
                android:padding="4dp"
                android:text="ClipGridLayout" />

            <com.gitee.hljdrl.clip.ClipGridLayout
                android:layout_width="200dp"
                android:layout_height="200dp"
                android:layout_gravity="center_horizontal"
                android:columnCount="2"
                android:rowCount="2"
                app:clipRadius="20dp">

                <View
                    android:layout_width="100dp"
                    android:layout_height="100dp"
                    android:background="#00ff00" />

                <View
                    android:layout_width="100dp"
                    android:layout_height="100dp"
                    android:background="#009688" />

                <View
                    android:layout_width="100dp"
                    android:layout_height="100dp"
                    android:background="#FFC107" />

                <View
                    android:layout_width="100dp"
                    android:layout_height="100dp"
                    android:background="#3F51B5" />
            </com.gitee.hljdrl.clip.ClipGridLayout>


            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="10dp"
                android:layout_marginBottom="10dp"
                android:background="@android:color/darker_gray"
                android:padding="4dp"
                android:text="ClipTableLayout" />

            <com.gitee.hljdrl.clip.ClipTableLayout
                android:layout_width="300dp"
                android:layout_height="wrap_content"
                android:layout_gravity="center_horizontal"
                android:layout_marginBottom="20dp"
                android:orientation="horizontal"
                app:bottomLeftRadius="20dp"
                app:bottomRightRadius="30dp"
                app:topLeftRadius="50dp"
                app:topRightRadius="10dp">

                <View
                    android:layout_width="100dp"
                    android:layout_height="100dp"
                    android:background="#00ff00" />

                <View
                    android:layout_width="100dp"
                    android:layout_height="100dp"
                    android:background="#009688" />

                <View
                    android:layout_width="100dp"
                    android:layout_height="100dp"
                    android:background="#FFC107" />

                <View
                    android:layout_width="100dp"
                    android:layout_height="100dp"
                    android:background="#3F51B5" />
            </com.gitee.hljdrl.clip.ClipTableLayout>


        </LinearLayout>

    </ScrollView>

</RelativeLayout>
```






